# Deliverables - Milestone ML1

This folder contains the two public PROXIMITY deliverables generated at milestone ML1 of the project. 


## Deliverable D1.1: Use case requirements and non‐functional properties

This deliverable presents the PROXIMITY use case scenarios, their key requirements and the potential locations selected for the implementation of the project pilot.

## Deliverable D1.2: PROXIMITY platform definition

This deliverable presents the PROXIMITY platform definition and its requirements with respect to communication, computing, orchestration and monitoring capabilities.